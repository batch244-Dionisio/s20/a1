
/*
3. Create a variable number that will store the value of the number provided by the user via prompt.

4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

5. Create a condition that if the current value is less than or equal to 50, stop the loop.

6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

7. Create another condition that if the current value is divisible by 5, print the number.

*/

let oneH = Number(prompt("Give me a number"));
		console.log('The number you provided is ' + oneH + '.')

	for (let oneH = 100; oneH >= 50; oneH -= 5){
		if (oneH % 10 === 0 && oneH !== 50){
			console.log('The number is divisible by 10. Skipping the number.')
			continue;
		}if (oneH == 50){
			console.log('The current value is at 50. Terminating the loop.');
			break;
		}if (oneH % 5 === 0){
			console.log(oneH);
			continue;
		}
	}	



/*

8. Create a variable that will contain the string supercalifragilisticexpialidocious.

9. Create another variable that will store the consonants from the string.

10. Create another for loop that will iterate through the individual letters of the string based on its length.

11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

12. Create an else statement that will add the letter to the second variable.

*/



	let supCali = "supercalifragilisticexpialidocious";
	let vowel = [ 'a', 'e', 'i', 'o', 'u', ];
	let isVowel,
		removed = '';

		for (let counter = 0; counter < supCali.length; counter++){
			isVowel = vowel.includes(supCali[counter]);

			if (isVowel === true) {
				continue;
			}else {
				removed += supCali[counter];
			}
		}

	console.log(supCali)
	console.log(removed)		